#!/usr/bin/env python

from __future__ import print_function
import time
from pymavlink import mavutil
#from pymavlink import mavlinkv10 as mavlink

def wait_heartbeat(m):
    '''wait for a heartbeat so we know the target system IDs'''
    print("Waiting for APM heartbeat")
    m.wait_heartbeat()
    print("Heartbeat from APM (system %u component %u)" % (m.target_system, m.target_system))


mavlink = mavutil.mavlink_connection('tcp:localhost:5760', planner_format=False,
                                  notimestamps=True,
                                  robust_parsing=True,
                                  source_system=234, source_component=0
                                  )

wait_heartbeat(mavlink)

# doesn't seems to be required
# mavlink.mav.request_data_stream_send(master.target_system, master.target_component, mavutil.mavlink.MAV_DATA_STREAM_ALL, args.rate, 1)


#mavlink.mav.heartbeat_send(1, 1)
#mavlink.mav.named_value_int_send(name="FireClass", value=123, time_boot_ms=5778)


while True:
    l = mavlink.recv_match();
    if l is not None:
       l_last_timestamp = 0
       if  l.get_type() != 'BAD_DATA':
           l_timestamp = getattr(l, '_timestamp', None)
           if not l_timestamp:
               l_timestamp = l_last_timestamp
           l_last_timestamp = l_timestamp

       print("--> %s.%02u: %s\n" % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(l._timestamp)),
           int(l._timestamp*100.0)%100, l))

#       mavlink.mav.named_value_int_send(name="FireClass".encode(), value=int(l._timestamp*100.0)%5, time_boot_ms=5778)
#       mavlink.mav.named_value_int_send(name="FireAgent".encode(), value=int(l._timestamp*100.0)%3, time_boot_ms=5778)

       #print(l);
       
       
