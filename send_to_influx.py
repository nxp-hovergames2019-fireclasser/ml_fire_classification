#!/usr/bin/env python

from __future__ import print_function
import time
import datetime
from pymavlink import mavutil
from influxdb import InfluxDBClient


noData = 0; # show no measurements as 0 value 
measurements = {}
sensors = ['410nm', '435nm', '460nm', '485nm', '510nm', '535nm', '560nm', '585nm', '610nm', '645nm', '680nm', '705nm', '730nm', '760nm', '810nm', '860nm', '900nm', '940nm', 'H2', 'Ethanol', 'TVOC', 'NH3', 'CO', 'NO2', 'C3H8', 'C4H10', 'CH4', 'CO2', 'SensTemp', 'RH']

def process_message(m):
    measurements[l.name] = l.value

def valuesOnly(measuredValues):
    values = []
    for sensor in sensors:
      values.append(measurements.get(sensor, noData))
    return values

def CSV(values):
#    return ','.join(map(str, values))
    return ",".join(map("{0:.2f}".format, values))

def sendToInflux(data):
    json_body = [
        {
            "measurement": "nxpdrone",
            "tags": {
                "drone": "nxp01",
            },
            "time": datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z',
            "fields": data
        }
    ]

    return client.write_points(json_body)

client = InfluxDBClient('localhost', 8086, 'root', 'root', 'k6')

#mavlink = mavutil.mavlink_connection('/dev/ttyUSB1', baud=57600,
mavlink = mavutil.mavlink_connection('tcp:localhost:5760', 
                                  planner_format=False,
                                  notimestamps=True,
                                  robust_parsing=True,
                                  source_system=234, source_component=0
                                  )

while True:
    l = mavlink.recv_match();
    if l is not None:
       l_last_timestamp = 0
       if  l.get_type() != 'BAD_DATA':
           l_timestamp = getattr(l, '_timestamp', None)
           if not l_timestamp:
               l_timestamp = l_last_timestamp
           l_last_timestamp = l_timestamp
           if (l.get_type() == 'NAMED_VALUE_FLOAT'): process_message(l)
           if (l.get_type() == 'NAMED_VALUE_INT'): 
               process_message(l)
               if (l.name == "Measured"): 
                   sendToInflux(measurements)
                   #print(CSV(valuesOnly(measurements)))

       #print("--> %s.%02u: %s\n" % (time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(l._timestamp)),
       #    int(l._timestamp*100.0)%100, l))
       #print(l);
       
