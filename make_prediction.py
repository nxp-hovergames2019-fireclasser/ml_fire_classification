#!/usr/bin/env python

from __future__ import absolute_import, division, print_function, unicode_literals
import os

#os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import time
from pymavlink import mavutil

import tensorflow as tf;
print("TensorFlow version: {}".format(tf.__version__))
print("Eager execution: {}".format(tf.executing_eagerly()))

from tensorflow import keras
#from tensorflow.keras.models import load_model

import warnings
warnings.filterwarnings('ignore')

noData = 0; # show no measurements as 0 value 
measurements = {}
sensors = ['410nm', '435nm', '460nm', '485nm', '510nm', '535nm', '560nm', '585nm', '610nm', '645nm', '680nm', '705nm', '730nm', '760nm', '810nm', '860nm', '900nm', '940nm', 'H2', 'Ethanol', 'TVOC', 'NH3', 'CO', 'NO2', 'C3H8', 'C4H10', 'CH4', 'CO2', 'SensTemp', 'RH']

class_names = ['Not Detected', 'Wood', 'Candle']
model = keras.models.load_model('my_model.h5')

test_set = [
    [1450.82,553.79,948.55,442.83,655.52,806.57,362.26,412.39,1135.77,247.28,388.21,69.81,103.39,74.93,157.61,380.79,144.40,67.47,13972.00,17659.00,1.00,5.81,3.53,0.07,14599.71,6195.26,282.35,1711.00,26.38,42.01],
    [1569.30,592.95,956.07,454.21,630.83,775.13,350.63,400.87,1088.45,239.64,371.00,70.60,105.13,76.58,140.20,342.12,146.81,76.84,12239.00,16698.00,1567.00,5.10,20.48,0.82,11897.34,12088.24,170155.48,9480.00,37.85,28.24],
    [1439.17,556.59,952.31,447.57,645.05,793.71,357.41,408.07,1113.23,244.31,376.38,70.60,105.13,76.58,151.20,364.38,146.21,72.15,13809.00,17743.00,13.00,1.13,3.53,0.45,1287.24,765.34,282.35,3204.00,36.31,30.01],
    [1540.16,574.30,954.19,448.52,645.05,793.71,357.90,408.07,1112.11,244.73,373.15,69.81,103.39,74.11,146.62,359.70,143.79,70.28,13523.00,16436.00,2854.00,108.58,13.19,0.04,1124697.75,263286.88,27271.54,2495.00,37.09,27.43],
]

def process_message(m):
    measurements[l.name] = l.value

def valuesOnly(measuredValues):
    values = []
    for sensor in sensors:
      values.append(measurements.get(sensor, noData))
    return values

def CSV(values):
    #return ','.join(map(str, values))
    return ",".join(map("{0:.2f}".format, values))

def percentages(n): 
    return "{: 6.1f}%".format(100*n)

def inferFireClass(values):
    predict_dataset = tf.convert_to_tensor([values])
    #print(predict_dataset)
    predictions = model(predict_dataset)
    #print(predictions)
    return predictions

def printReadableOutput(results):
    for i, logits in enumerate(results):
      results = map(percentages, logits)
 
      class_idx = tf.argmax(logits).numpy()
      p = tf.nn.softmax(logits)[class_idx]
      name = class_names[class_idx]
      print("{} \t Sample #{} prediction: {} ({:4.1f}%)".format(', '.join(results), i, name, 100*p))

def sendResults(results, time):
    for i, logits in enumerate(results):
      results = map(percentages, logits)

      class_idx = tf.argmax(logits).numpy()
      p = tf.nn.softmax(logits)[class_idx]
      name = class_names[class_idx]
#      print(class_idx.__class_)  AttributeError: 'numpy.int64' object has no attribute '__class_'
#      mavlink.mav.named_value_int_send(name="FireClass", value=class_idx, time_boot_ms=123)
      mavlink.mav.named_value_int_send(name="FireClass".encode(), value=class_idx, time_boot_ms=time)
      agent4class = [0,1,2,3,3,4,5]
      agent_idx = agent4class[class_idx]
      mavlink.mav.named_value_int_send(name="FireAgent".encode(), value=agent_idx, time_boot_ms=time)
#      print(class_idx)
#      print(agent_idx)
  

#mavlink = mavutil.mavlink_connection('/dev/ttyUSB1', baud=57600,
mavlink = mavutil.mavlink_connection('tcp:192.168.85.103:5760', 
                                  planner_format=False,
                                  notimestamps=True,
                                  robust_parsing=True,
                                  source_system=234, source_component=0
                                  )
print(',  '.join(class_names))
print_all_messages = False

while True:
    l = mavlink.recv_match();
    if l is not None:
       if print_all_messages: print(l)
       l_last_timestamp = 0
       if  l.get_type() != 'BAD_DATA':
           l_timestamp = getattr(l, '_timestamp', None)
           if not l_timestamp:
               l_timestamp = l_last_timestamp
           l_last_timestamp = l_timestamp
           if (l.get_type() == 'NAMED_VALUE_FLOAT'): process_message(l)
           if (l.get_type() == 'NAMED_VALUE_INT'): 
               process_message(l)
               if (l.name == "Measured"): 
                   values = valuesOnly(measurements)
                   #values = test_set[2]
                   print(CSV(values))
                   results =  inferFireClass(values)
                   printReadableOutput(results)
                   sendResults(results, l_last_timestamp)


